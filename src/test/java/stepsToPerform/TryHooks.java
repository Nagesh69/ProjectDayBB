package stepsToPerform;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class TryHooks extends SeMethods {
	@Before
	public void beforeCucumber(Scenario Sc) {
		startResult();
		testCaseName = Sc.getName();
		testCaseDescription =Sc.getId();
		category = "Smoke";
		author= "Babu";
		startTestCase();
		startApp("Chrome", "http://leaftaps.com/opentaps");
	}
		
	
}
