package pagesBB;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class DOBPage extends ProjectMethods {
	
	public IncomePage SelAge() {
		int age=24;
		Actions builder=new Actions(driver);
		WebElement slider = driver.findElementByXPath("//div[@class= 'rangeslider__handle-label']");
		builder.dragAndDropBy(slider, (age-18)*8, 0).perform();
		
		WebElement MonthClick = locateElement("linktext", "Feb 1994");
		click(MonthClick);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement DayClick = locateElement("xpath", "//div[@class='react-datepicker__day react-datepicker__day--sat react-datepicker__day--weekend']");
		click(DayClick);
		
		WebElement PrintDOB = locateElement("xpath", "(//span[@class='Calendar_highlight_xftqk'])[3]");
		System.out.println(PrintDOB.getText());
		
		WebElement clickCont = locateElement("linktext", "Continue");
		click(clickCont);
		
		return new IncomePage();
		
	
		
		
	}

}
