package pagesBB;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	public MutualFunds mousehoverInvestment() {
		Actions builder=new Actions(driver);
		WebElement hover = driver.findElementByLinkText("INVESTMENTS");
		builder.moveToElement(hover).perform();
		WebElement mutualFunds = driver.findElementByLinkText("Mutual Funds");
		click(mutualFunds);
		return new MutualFunds();
	}
}
