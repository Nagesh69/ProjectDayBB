package pagesBB;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class IncomePage extends ProjectMethods {
	
	public InvestFundsList selectIncome(){
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Actions builder=new Actions(driver);
		WebElement slider = driver.findElementByXPath("//div[@class= 'rangeslider__handle-label']");
		builder.dragAndDropBy(slider, 126, 0).perform();
		
		WebElement clickCont = locateElement("linktext", "Continue");
		click(clickCont);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement primBank = locateElement("xpath","//span[text()='SBI']");
		click(primBank);
		WebElement inputName = locateElement("name", "firstName");
		inputName.sendKeys("Nagesh");
		
		
		WebElement viewMutFunds = locateElement("linktext", "View Mutual Funds");
		click(viewMutFunds);
		
		return new InvestFundsList();
		
		
		
		
		
		
		
		
	}

}
