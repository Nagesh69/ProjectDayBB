package testCasesBB;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import pagesBB.HomePage;
import wdMethods.ProjectMethods;

public class TC001_HomePage extends ProjectMethods {

	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC001_HomePage";
		testCaseDescription ="BB HomePage";
		category = "Smoke";
		author= "Babu";
		//dataSheetName="TC001";
	}
	@Test
	public  void HomePageBB()   {
		new HomePage()
		.mousehoverInvestment()
		.SearchMutFund()
		.SelAge()
		.selectIncome()
		.listOfSchemes();
		
		
	}
	
}
